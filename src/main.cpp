#include <Arduino.h>
#include <Wire.h>
#include <vl53lx_class.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <stdlib.h>
#include "vl53lx.h"
#include <analogWrite.h>

const int pwm_pin = 2;
const int turn_off_pin = 4;

void setup()
{
  Serial.begin(115200);
  pinMode(turn_off_pin, INPUT_PULLUP);
  analogWriteResolution(pwm_pin, 13);
  analogWriteFrequency(10000);
  setup_vl53lx();
}

void loop()
{
  if (digitalRead(turn_off_pin))
  {
    analogWrite(pwm_pin, 0, 255);
  }
  else
  {
    int dist_ms = vl53lx_measurement();
    Serial.print(dist_ms);
    Serial.println(" mm");

    float scaled_dist = 1. - min(1.f, (float)(dist_ms) / 2000.f);
    int offset = 30;
    int max_val = 255;
    int duty_cycle = (int)(scaled_dist * (max_val - offset) + offset);

    Serial.print("duty: ");
    Serial.println(duty_cycle);
    analogWrite(pwm_pin, duty_cycle, 255);
  }
}
