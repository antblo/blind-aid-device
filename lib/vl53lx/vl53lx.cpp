#include <Arduino.h>
#include <vl53lx_class.h>
#include "vl53lx.h"

#define DEV_I2C Wire
#define SerialPort Serial
VL53LX sensor_vl53lx_sat(&DEV_I2C, 17);

const int interrupt_pin = 32;
const int x_shut = 33;

void setup_vl53lx()
{
    Wire.begin();
    pinMode(interrupt_pin, INPUT);
    pinMode(x_shut, OUTPUT);
    digitalWrite(x_shut, HIGH);
    sensor_vl53lx_sat.begin();
    sensor_vl53lx_sat.VL53LX_Off();
    const int address = 0x12;
    sensor_vl53lx_sat.InitSensor(address);
    sensor_vl53lx_sat.VL53LX_StartMeasurement();
}

int vl53lx_measurement()
{
    VL53LX_MultiRangingData_t MultiRangingData;
    VL53LX_MultiRangingData_t *pMultiRangingData = &MultiRangingData;
    int status = 0;

    status = sensor_vl53lx_sat.VL53LX_GetMultiRangingData(pMultiRangingData);
    int no_of_object_found = pMultiRangingData->NumberOfObjectsFound;
    int min_dist = 4000;
    for (int j = 0; j < no_of_object_found; j++)
    {
        if (pMultiRangingData->RangeData[j].RangeStatus != 0)
            continue;
        int dist = pMultiRangingData->RangeData[j].RangeMilliMeter;
        if (min_dist > dist)
            min_dist = dist;
    }
    if (status == 0)
    {
        status = sensor_vl53lx_sat.VL53LX_ClearInterruptAndStartMeasurement();
    }
    uint8_t not_ready = 1;
    while (not_ready)
    {
        not_ready = digitalRead(interrupt_pin);
    }
    return min_dist;
}
